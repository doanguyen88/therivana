import React from "react";
import Slider from '../../components/slider/slider';

import img01 from '../../assets/catalog/background/bg.jpg';
import img02 from '../../assets/catalog/background/bg2.jpg';
import img03 from '../../assets/catalog/background/bg3.jpg';
import img04 from '../../assets/catalog/background/bg4.jpg';
import img05 from '../../assets/catalog/background/bg5.jpg';
import img06 from '../../assets/catalog/background/bg6.jpg';
import img07 from '../../assets/catalog/background/bg6.jpg';


import './home.scss';

const products = [
  {
    name: 'Rau củ quả vừa',
    description: 'size vừa',
    image: img01
  },
  {
    name: 'Rau củ quả lớn',
    description: 'size lớn',
    image: img02
  },
  {
    name: 'Rau củ quả nhỏ',
    description: 'size nhỏ',
    image: img03
  },
  {
    name: 'Rau củ quả nhỏ',
    description: 'size nhỏ',
    image: img04
  },
  {
    name: 'Rau củ quả nhỏ',
    description: 'size nhỏ',
    image: img05
  },
  {
    name: 'Rau củ quả nhỏ',
    description: 'size nhỏ',
    image: img06
  },
  {
    name: 'Rau củ quả nhỏ',
    description: 'size nhỏ',
    image: img07
  },
]

const Home = () => {
  return <div className="homepage">
    <div className="slider_main_place">
        <Slider images={products.map(p => p.image)} showThumbnail={false} />
        <div className="wheel show">
            <span></span>
            <span></span>
        </div>
    </div>
  </div>
}

export default Home;