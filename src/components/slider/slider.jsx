import React, { useState } from "react";
import BtnSlider from "./btn_slider";
import dataSlider from "./data_slider";
import "./slider.css";

function Slider({ images = [], showThumbnail = true }) {
  const [slideIndex, setSlideIndex] = useState(1);

  const nextSlide = () => {
    if (slideIndex !== dataSlider.length) {
      setSlideIndex(slideIndex + 1);
    } else if (slideIndex === dataSlider.length) {
      setSlideIndex(1);
    }
  };

  const prevSlide = () => {
    if (slideIndex !== 1) {
      setSlideIndex(slideIndex - 1);
    } else if (slideIndex === 1) {
      setSlideIndex(dataSlider.length);
    }
  };

  const moveDot = index => {
    setSlideIndex(index)
  }


  return (
    <div className="container-slider">
      {images.map((item, index) => (
          <div
            key={index}
            className={
              slideIndex === index + 1 ? "slide active-anim" : "slide"
            }>
            <img id={'myimage' + index} className='pure-image' src={item} alt='' width={'100%'} height={'100%'}/>
          </div>
        // );
      ))}
      {/* <BtnSlider moveSlide={nextSlide} direction={"next"} /> */}
      {/* <BtnSlider moveSlide={prevSlide} direction={"prev"} /> */}

      <div className="container-dots">
        {Array.from({length: 7}).map((item, index) => (
          <div key={index} onClick={() => moveDot(index + 1)}
            className={slideIndex === index + 1 ? "dot active" : "dot"}></div>
        ))}
      </div>
    </div>
  );
}

export default Slider;