import React from 'react'
import './header.css'

import logo from '../../assets/images/logo.svg'
import batram60do from '../../assets/images/360.svg'
import subcribe from '../../assets/images/subscribe.svg'


const Header = () => {
  return (
    <div className='header__container'>
        <div className='header__logo'>
            <img className='logo__icon' src={logo} alt='logo' />
        </div>
        <div className='header__nav'>
            <div className='header__360 header_item'>
                <img src={batram60do} />
                <span>360º</span>
            </div>
            <div className='header__sub header_item'>
                <img src={subcribe} />
                {/* <span>SUBCRIBE(Yêu cầu tư vấn)</span> */}
                <span>Yêu cầu tư vấn</span>
            </div>
            <div className='header__lang header_item'>
                {/* <span>Language(VI/EN)</span> */}
                <span>EN</span>
            </div>
            <div className='header__menu header_item'>
                <span>MENU</span>
            </div>
        </div>
    </div>
  )
}

export default Header;