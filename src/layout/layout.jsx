import React from 'react'
import Body from './body/body'
import Header from './header/header'
import Footer from './footer/footer'

import { Router } from '../router'

const Layout = (props) => {
  return (
    <div className='layout-wrapper'>
      <Header />
      <Body>
        <Router />
      </Body>
      <Footer />
    </div>
  )
}

export default Layout;
